 /**
 * (C)VIOLONIX inc.
 * Parser for make multidim array from
 * foo[]=any&foo[]=boy, or foo[0][kids]=any&foo[1][kids]=boy
 * result: foo=[[any],[boy]] or foo=[kids:[any],kids:[boy]]
 */ 

 import axios from 'axios';

const URLToArray = function(url) {
    function parse_mdim(name, val, data){
        
        let params = name.match(/(\[\])|(\[.+?\])/g);
        if(!params)params = new Array();
        let tg_id = name.split('[')[0];


        if(!(tg_id in data)) data[tg_id] = [];
        var prev_data = data[tg_id];

        for(var i=0;i<params.length;i++){
            if(params[i]!='[]'){
                let tparam = params[i].match(/\[(.+)\]/i)[1];
                if(!(tparam in prev_data)) prev_data[tparam] = [];
                prev_data = prev_data[tparam];
            }else{
                prev_data.push([]);
                prev_data = prev_data[prev_data.length-1];
            }

        }
        prev_data.push(val);

    }


    var request = {};
    var arr = [];
    var pairs = url.substring(url.indexOf('?') + 1).split('&');

    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        if(decodeURIComponent(pair[0]).indexOf('[')!=-1)
            parse_mdim(decodeURIComponent(pair[0]), decodeURIComponent(pair[1]), request);
        else 
            request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }

    //To-do here check array and simplifity it: if parameter end with one index in array replace it by value [0]    

    return request;

}

const lineItemsFromURL = function( cart_items ){
    const line_items = [];
    cart_items.forEach( item => {
        let new_object = {};
        Object.keys( item ).forEach( value => {
            new_object[value] = JSON.stringify(item[value]).slice(2 , -2)
        })
        line_items.push( new_object )
    })
    return line_items
}

const getDataLayerProductsData = function(lineItems) {
    return new Promise((resolve, reject) => {
        let productsData = []
        let promises = []
        //const shopifyShop = 'siempreencasadev'
        const shopifyShop = 'siempre-en-casa'
        lineItems.forEach(item  => {
            var url = `https://${shopifyShop}.myshopify.com/products/${item.handle}.json`
            promises.push(axios
                .get(url)
                .then((response) => {
                    let product = response.data.product
                    let variant = product.variants.filter( variant => variant.id === parseInt(item.variant_id))[0]
                    productsData.push({
                        'id': product.id,
                        'name': product.title,
                        'brand': product.vendor,
                        'list': null,
                        'category': product.product_type,
                        'variant': variant.title,
                        'metric1': parseFloat(variant.compare_at_price) - parseFloat(variant.price),
                        'price': parseFloat(variant.price),
                        'quantity': parseInt(item.quantity)
                    })
                }))
        })
        axios.all(promises).then(function(){
            resolve(productsData)
        })
    })
};

const getSubTotal = function(lineItems) {
    let subtotal = 0;
    lineItems.map( item => { subtotal = parseFloat(subtotal) + ( parseFloat( item.price ) * item.quantity ) })
    return subtotal;
} 

const errorsList = {
    205 : {
        'element_id' : 'cardNumber-helper-block',
        'text' : 'El número de la tarjeta no puede estar vacío'
    },
    208 : {
        'element_id' : 'cardExpirationMonth-helper-block',
        'text' : 'El mes de vencimiento no puede estar vacío'
    },
    209 : {
        'element_id' : 'cardExpirationYear-helper-block',
        'text': 'El año de vencimiento no puede estar vacío'
    },
    221 : {
        'element_id' : 'cardholderName-helper-block',
        'text': 'Ingresa el nombre y apellido'
    },
    224 : {
        'element_id' : 'securityCode-helper-block',
        'text' : 'Ingresa el código de seguridad'
    },
    325 : {
        'element_id' : 'cardExpirationMonth-helper-block',
        'text' : 'El mes de vencimiento es incorrecto'
    },
    326 : {
        'element_id' : 'cardExpirationYear-helper-block',
        'text': 'El año de vencimiento es incorrecto'
    },
    E301 : {
        'element_id' : 'cardNumber-helper-block',
        'text' : 'El número de tarjeta es incorrecto'
    },
    E302 : {
        'element_id' : 'securityCode-helper-block',
        'text' : 'Revisa el código de seguridad.'
    },
    316 : {
        'element_id' : 'cardholderName-helper-block',
        'text': 'Ingresa un nombre válido'
    }
};

//const ApiURL = 'https://devsiempreencasacheckout.com/api/v1/';

const ApiURL = 'https://siempreencasacheckout.com/api/v1/';


export { URLToArray , lineItemsFromURL, errorsList , ApiURL, getDataLayerProductsData, getSubTotal };