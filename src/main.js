import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueAnalytics from 'vue-analytics'

import "@/assets/css/custom.scss"

Vue.use(BootstrapVue)

import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

Vue.filter('currencyARS', function (value) {
  if (!value) return ''
  value = parseFloat(value) / 100
  return new Intl.NumberFormat('ES-AR', { style: 'currency', currency: 'ARS' }).format( value ) 
})



Vue.use(VueAnalytics, {
  id: 'UA-144026747-1',
  disableScriptLoader: true,
  ecommerce: {
    enabled: true,
    enhanced: true
  },
  autoTracking: {
    exception: true,
    exceptionLogs: false
  },
  linkers: ['https://siempreencasa.com.ar', 'https://siempreencasacheckout.com']
})

import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

Sentry.init({
  dsn: 'https://7502912682cf4b708fb22d83491cfd34@o348957.ingest.sentry.io/5194190',
  integrations: [new Integrations.Vue({Vue, attachProps: true})],
});


/*
Vue.use(VueGtm, {
  id: 'GTM-TCP9WP4',
  enabled: true, // defaults to true. Plugin can be disabled by setting this to false for Ex: enabled: !!GDPR_Cookie (optional)
  debug: true, // Whether or not display console logs debugs (optional)
  loadScript: true, // Whether or not to load the GTM Script (Helpful if you are including GTM manually, but need the dataLayer functionality in your components) (optional) 
  vueRouter: router // Pass the router instance to automatically sync with router (optional)
  //ignoredViews: ['homepage'] // If router, you can exclude some routes name (case insensitive) (optional)
})
*/

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')


