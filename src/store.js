import Vue from 'vue'
import Vuex from 'vuex'
import { getField, updateField } from 'vuex-map-fields';

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    order: {
      id: null,
      step: 1,
      total_price: 0,
      buyer_accepts_marketing: true,
      suscription_id: null,
      suscription_title: null,
      token: '',
      attributes: {
        goal: false,
        message: '',
        depto: '',
        flat: '',
        delivery_hour: '',
        lat: '',
        lng: '',
        urlGoogleMap: ''
      }
    },
    customer : {
      first_name: '',
      last_name: '',
      dni: null,
      phone: '',
      sex: null,
      birthdate: '',
      email: '',
      address: '',
      city: '',
      zip_code: '',
      province: '',
      country: "Argentina"
    },
    shipping: {
      id: null,
      order_id : null,
      range_id : null,
      name : '',
      min_order_subtotal: 0,
      price : 0,
      max_order_subtotal : 0,
      shipping_zone_id : 0
    },
    line_items: [],
    shipping_zones : [],
    discount: {
      id: null,
      code: null,
      price_rule_id : null,
      discount_code_id : null,
      type : null,
      value : null,
      title : null,
      target : null,
      show_value: 0
    },
    mercado_pago: {
      public_key: null
    },
    creditCardPlugin: null,
    suscription: {
      id: null,
      title: null
    }
  },
  mutations: {

    updateField,

    SET_ORDER_ID(state, field) {
      state.order.id = field
    },

    SET_ORDER_STEP(state, field) {
      state.order.step = field
    },

    SET_ORDER_TOTAL_PRICE(state, field) {
      state.order.total_price = field
    },

    SET_LINE_ITEMS(state, field) {
      state.line_items = field
    },

    SET_CUSTOMER( state, field ){
      state.customer.first_name = field.first_name
      state.customer.last_name = field.last_name
      state.customer.dni = field.dni
      state.customer.phone = field.phone
      state.customer.sex = null
      state.customer.birthdate = field.birthdate
      state.customer.address = field.address
      state.customer.city = field.city
      state.customer.zip_code = field.zip_code
      state.customer.province = field.province
    },
    
    SET_SHIPPING_ID(state, field) {
      state.shipping.id = field
    },

    SET_SHIPPING(state, field) {
      state.shipping.order_id = state.order.id
      state.shipping.range_id = field.shipping_zone_id
      state.shipping.name = field.name
      state.shipping.min_order_subtotal = field.min_order_subtotal
      state.shipping.price = field.price
      state.shipping.max_order_subtotal = field.max_order_subtotal
      state.shipping.shipping_zone_id = field.shipping_zone_id
    },

    SET_SHIPPING_ZONES(state, field) {
      state.shipping_zones = field
    },

    SET_DISCOUNT(state, field) {
      state.discount = field
    },

    SET_DISCOUNT_ID(state, field) {
      state.discount.id = field
    },

    SET_DISCOUNT_SHOW_VALUE(state, field) {
      state.discount.show_value = field
    },

    RESET_DISCOUNT( state, field ){
      state.discount.id = null
      state.discount.code = null
      state.discount.price_rule_id = null
      state.discount.discount_code_id = null
      state.discount.type = null
      state.discount.value = null
      state.discount.title = null
      state.discount.target = null
      state.discount.show_value = 0
    },

    SET_MP_PUBLIC_KEY(state, field) {
      state.mercado_pago.public_key = field
    }


  },
  getters: {
    getField,
  },
  actions: {
  },

})
